% Gallerie GitLab-CI
% Journée des Ingénieurs

---

## Plateformes Outils

[https://ci.inria.fr](https://ci.inria.fr) + [https://gitlab.inria.fr](https://gitlab.inria.fr)

---

## GitLab

De nombreux "outils" autour de cette forge:

 * CI/CD
 * Registry: containers, packages, ...
 * pages
 * shared runners
 * ...

---

## GitLab-CI

Un fichier de configuration `.gitlab-ci.yml`:

 * à la racine d'un dépôt git
 * peut inclure d'autres fichiers `.yml`
 * *macros*
 * très bonne documentation

---

## [Gallerie GitLab-CI](https://gitlab.inria.fr/gitlabci_gallery/gitlab-profile/-/blob/main/README.md), mais pourquoi ?

 * promotion des outils internes
 * formation
 * copier/coller

---

## [LaTeX collaboratif](https://gitlab.inria.fr/gitlabci_gallery/latex/latex-beamer)

```mermaid
%%{init: { 'theme': 'dark', 'gitGraph': {'showBranches': true, 'showCommitLabel':false }} }%%
gitGraph
    commit
    commit
    branch intro
    branch conclusion
    checkout intro
    commit
    checkout conclusion
    commit
    checkout intro
    commit
    checkout conclusion
    commit
    checkout main
    merge intro
    checkout conclusion
    commit
    checkout main
    merge conclusion
```

---

## [Docker in docker](https://gitlab.inria.fr/gitlabci_gallery/docker/statistics/-/blob/main/.gitlab-ci.yml?ref_type=heads)

```mermaid
%%{init: { 'theme': 'dark', 'gitGraph': {'showBranches': true, 'showCommitLabel':false }} }%%
gitGraph
    commit
    commit
    branch develop
    checkout develop
    commit tag: "project:develop"
    commit
    checkout main
    merge develop tag: "rm project:develop" type: REVERSE
    commit
    commit
```

---

## [API cloudstack](https://gitlab.inria.fr/gitlabci_gallery/orchestration/cloudstack-api)

 * Créer / Démarrer / Arrêter des VMs sur Cloudstack

---

## Orchestration

 * Création de VMs avec TerraForm
 * CI sur cluster de calcul PlaFrim

---

## [Custom runner](https://gitlab.inria.fr/vrouvrea/custom-runner)

 * en bêta
 * linux / osx / windows(wsl)
 * docker

---

## Et vous ?

 * [thématiques](https://gitlab.inria.fr/gitlabci_gallery) manquantes ?
 * Prêts à contribuer ?
