```mermaid
mindmap
  root((Gitlab-CI gallery))
    Gitlab (from 2015)
      Configuration
        Enable project CI/CD feature
        Enable shared runners (from 2018)
        Enable custom runners (bêta)
    CI/CD
      .gitlab-ci.yml
    Gallery
      why ?
      BSD-3 license
      simple latex example
    Executors
      shared docker runners
        docker machine is deprecated
      https://ci.inria.feature
      custom runner
    Pages
    Cache/Artefacts
    Pipeline
```